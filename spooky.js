var character, tpumpkin, tdonut, tcandy, tbread;
function spooky() {
    character = new Sprite(game, "ghost3.png", 50, 60);
    character.loadAnimation(234, 432, 78, 108);
    character.generateAnimationCycles();
    character.renameCycles(new Array("down", "left", "right", "up"));
    character.setAnimationSpeed(1500);

    //start paused
    character.setPosition(300, 320);
    character.setSpeed(0);
    character.playAnimation()
    // character.pauseAnimation();
    character.setCurrentCycle("down");

    character.checkKeys = function () {
        if (keysDown[K_LEFT]) {
            character.setSpeed(4);
            character.playAnimation()
            character.setMoveAngle(270);
            character.setCurrentCycle("left");
        }
        if (keysDown[K_RIGHT]) {
            character.setSpeed(4);
            character.playAnimation()
            character.setMoveAngle(90);
            character.setCurrentCycle("right");
        }
        if (keysDown[K_UP]) {
            character.setSpeed(4);
            character.playAnimation()
            character.setMoveAngle(0);
            character.setCurrentCycle("up");
        }
        if (keysDown[K_DOWN]) {
            character.setSpeed(4);
            character.playAnimation()
            character.setMoveAngle(180);
            character.setCurrentCycle("down");
        }

        if (keysDown[K_SPACE]) {
            character.setSpeed(0);
            character.playAnimation()
            // character.pauseAnimation();
            character.setCurrentCycle("down");
        }

    }
    return character;
}

function Pumpkin() {
    tpumpkin = new Sprite(game, "pumpkin.png", 40, 40);
    tpumpkin.setSpeed(7);
    tpumpkin.wriggle = function () {
        newDir = (Math.random() * 20) - 10;
        this.changeAngleBy(newDir);
    }
    tpumpkin.reset = function () {
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY)
    }
    return tpumpkin;
}

function Donut() {
    tdonut = new Sprite(game, "donut.png", 40, 40);
    tdonut.setSpeed(5);
    tdonut.wriggle = function () {
        newDir = (Math.random() * 20) - 10;
        this.changeAngleBy(newDir);
    }
    tdonut.reset = function () {
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY)
    }
    return tdonut;
}

function Candy() {
    tcandy = new Sprite(game, "candy.png", 40, 40);
    tcandy.setSpeed(5);
    tcandy.wriggle = function () {
        newDir = (Math.random() * 20) - 10;
        this.changeAngleBy(newDir);
    }
    tcandy.reset = function () {
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY)
    }
    return tcandy;
}

function Bread() {
    tbread = new Sprite(game, "bread.png", 40, 40);
    tbread.setSpeed(7);
    tbread.wriggle = function () {
        newDir = (Math.random() * 20) - 10;
        this.changeAngleBy(newDir);
    }
    tbread.reset = function () {
        newX = Math.random() * this.cWidth;
        newY = Math.random() * this.cHeight;
        this.setPosition(newX, newY)
    }
    return tbread;
}
